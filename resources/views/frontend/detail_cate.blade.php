@extends('frontend.master')
@section('main')

<style>
    .p-3{
        padding: 1rem!important;
    }
    .border{
        border: 5px solid #dee2e6!important;
        border-radius: 10px;
    }
    .anh{
        margin-right: 10px;
    }
    .mt-3{
        margin-bottom: 10px;
    }
    .tieude{
        border-bottom: 4px solid silver;
    }
    .tieude h4{
        margin-bottom: -5px;
        background: white;
        display: inline-block;
        position: relative;
        top: 15px;
        left: 30px;
        padding-left: 15px;
        padding-right: 10px;
    }
    .entry-view{
        padding-right: 15px;
        padding-left:5px;
        font-size: 14px;
        color: #9f9f9f;
    }
   
</style>
        <div class="container">
            <div class="tieude">
             <h4>{{ $catename->cate_name }}</h4>  
            </div>
        </div>

       
            @foreach ($post_cate as $post)
            <div class="container mt-9 wow fadeInUp" data-wow-delay="0s"">
                <div class="row">     
                <div class="media border p-9">
                       
                <img src="{{ asset('storage/app/img/'.$post->img )}}" alt="Card image cap" class="anh rounded-circle" style="width:120px;height:120px;">
                <div class="media-body">
                        
                    <a href="{{ asset('post/'.$post->id.'/'.$post->slug.'.html')}}"><h4 style="margin-bottom: .5rem;
                        font-family: inherit; 
                        font-weight: 500;
                        line-height: 1.2;
                        color: inherit;">{{ $post->title }} </h4></a>
                        <div class="entry-meta">
                           
                        </div>
                    <p>{{ $post->quote }}</p>      
                </div>
                </div>
            </div>
            </div>
            @endforeach


@stop