@extends('frontend.master')
@section('main')

	
	<div id="body">
			@if(empty(Auth::user()->email))
				<a href="{{ asset('login') }}"><h1><span>Login</span></h1></a>
			
			@else
				<a href=""><h1><span>{{ Auth::user()->email }}</span></h1></a>
					<li>
							<a href="{{ asset('logout') }}"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a>
						</li>
				@endif
		
		<div>
			@foreach ($post as $post)
				<ul>
					
					<li>
							<a href="" class="figure">
								<img class="card-img-top img-fluid" src="{{ asset('storage/app/img/'.$post->img )}}" alt="Card image cap">
							</a>
							<div>
								<h3>{{ $post->title }}</h3>
								<p>
									{{ $post->quote }}
								</p>
								<a href="{{ route('postdetail',[$post->id,$post->slug])}} " class="more">read this</a>
							</div>
					</li>
					
					
					
				</ul>
			@endforeach
		</div>
	</div>

@stop