@extends('frontend.master')
@section('main')
<style>
		.p-3{
			padding: 0.5rem!important;
		}
		.border{
			border: 2px solid #dee2e6!important;
			border-radius: 5px;
		}
		.anh{
			margin-right: 10px;
		}
		.mt-3{
			margin-bottom: 10px;
		}
		.tieude{
			border-bottom: 4px solid silver;
		}
		.tieude h4{
			margin-bottom: -5px;
			background: white;
			display: inline-block;
			position: relative;
			top: 15px;
			left: 30px;
			padding-left: 15px;
			padding-right: 10px;
		}
		.noidungtin{
			background: #f7f7f7;
			padding-top: 20px;
			padding-bottom: 20px;
		}
		.tin{
			background: white;
			padding: 20px 10px 0px 30px; 
			border-radius: 10px;
		}
		.tieudetin1{
			font-size: 32px;
			color: black;
			line-height: 1.2;
			font-weight: bolder;
		}
		
		.entry-view{
			padding-right: 15px;
			padding-left:5px;
			font-size: 14px;
			color: #9f9f9f;
		}
		.flot-container {
			box-sizing: border-box;
			width: 100%;
			height: 275px;
			padding: 20px 15px 15px;
			margin: 15px auto 30px;
			background: transparent;
		}

	   
	</style>

		<section class="noidungtin ">
			<div class="container tin">
				<div class="row">
					<div class="col-sm-12">
						<div class="mottinchuan mb-3  wow  fadeInUp fontroboto">
						<hr>
						
						<img class="card-img-top img-fluid" src="{{ asset('storage/app/img/'.$post->img )}}" alt="Card image cap">
							<h2 class="tieudetin1">{{ $post->title }}</h2>
							<div class="entry-meta">
						
							</div>
							
							<p >{!! html_entity_decode($post->content) !!} </p>
							
						</div>
						

					</div> <!-- HET COT 12-->
					
				</div>
				<a href="{{{ asset('/like/'.$post->id) }}}"><span>like( {{ $like }})</span></a>
                <hr>
                <hr>
				
                <div class="comment-list">
                        @foreach ($comment as $comments)
                            <ul>
                                <li class="com-title">
                                    {{ $comments->name }}
                                    <br>
                                    <span>{{ date('Y-m-d H:i:s',strtotime($comments->created_at)) }}</span>	
                                </li>
                                <li class="com-details">
                                    {{ $comments->content }}
                                </li>
                            </ul>
                        @endforeach
                </div>
                <hr>
                <hr>
                <div class="comment">
                    <h3>Bình luận</h3>
                    <div class="col-md-9 comment">
                        <form method='post'>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input required type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="name">Tên:</label>
                                <input required type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="cm">Bình luận:</label>
                                <textarea required rows="10" id="cm" class="form-control" name="content"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-default">Gửi</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>

            </div>
            
			
								
        </section><!--  het noidung tin -->
        
        
		
		
		



		
		
		

@stop