@extends('backend.master')
@section('main')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
				<div class="col-lg-12">
						@if (Session::has('flash_message'))
							<div class="alert alert-success">
								{{ Session::get('flash_message') }}
							</div>
							
						@endif
					</div>
			<div class="col-sm-12 themmoi">
                    <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> Sửa BÀi VIẾT</h1>
                            </div>
                        </div><!--/.row-->
				<div class="formthemmoi">
                    <form action="" method="post" enctype="multipart/form-data">
                        <fieldset class="form-group">
                                <label>Danh mục</label>
                                <select required name="cate" class="form-control">
                                        @foreach ($catelist as $cate)
                                            <option value="{{ $cate->id }}" 
                                                @if ($post->post_cate == $cate->id) selected
                                                @endif>{{ $cate->cate_name }}
                                            </option>
                                        @endforeach
                                        
                                    </select>
                        </fieldset>
						<fieldset class="form-group">
							<label for="formGroupExampleInput">Tên đề bài viết </label>
							<input  required type="text" name="title" class="form-control" value="{{ $post->title }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="formGroupExampleInput">Trích dẫn </label>
                            <input  required type="text" name="quote" class="form-control" value="{{$post->quote  }}">
                        </fieldset>
                        <fieldset class="form-group" >
                            <label>Ảnh sản phẩm</label>
                            <input id="img" type="file" name="img" class="form-control " onchange="changeImg(this)">
                            <img id="avatar" class="thumbnail"  src="{{asset('storage/app/img/'.$post->img)}}" style="width:200px;height:200px">
                        </fieldset>
						<fieldset class="form-group">
							<label for="formGroupExampleInput">Nội dung tin </label>
							<textarea  required  name="content" id="content" cols="80" rows="10">
                                    {{ $post->content }}
                            </textarea>
                            <script>
								CKEDITOR.replace( 'content', {
								filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
								filebrowserImageBrowseUrl:  'ckfinder/ckfinder.html?Type=Images',

								filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
								filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
								filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
								filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								});
							</script>
						</fieldset>
						<fieldset class="form-group text-xs-center">	
                            <input class="form-control btn btn-primary" type="submit" value="Lưu">
                             <a href="{{ asset('admin/post') }}" class="form-control btn btn-danger" value="">Hủy</a>
                        </fieldset>
                       

						{{ csrf_field()}}
					</form>
				</div>


			</div>
			
        </div>
        <hr>
        <hr>
        
       

@stop