@extends('backend.master')
@section('main')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
				<div class="col-lg-12">
						@if (Session::has('flash_message'))
							<div class="alert alert-success">
								{{ Session::get('flash_message') }}
							</div>
							
						@endif
					</div>
			<div class="col-sm-12 themmoi">
                    <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">BÀi VIẾT</h1>
                            </div>
                        </div><!--/.row-->
				<div class="formthemmoi">
                    <form action="{{ asset('admin/post/add') }}" method="post" enctype="multipart/form-data">
                        <fieldset class="form-group">
                                <label>Danh mục</label>
                                <select required name="cate" class="form-control">
										
                                    @foreach ($catelist as $cate)
                                        <option value="{{ $cate->id }}">{{ $cate->cate_name }}</option>
                                    @endforeach
                                    
                                    
                                </select>
                        </fieldset>
						<fieldset class="form-group">
							<label for="formGroupExampleInput">Tên đề bài viết </label>
							<input  required type="text" name="title" class="form-control" id="">
                        </fieldset>
                        <fieldset class="form-group">
                                <label for="formGroupExampleInput">Trích dẫn </label>
                                <input  required type="text" name="quote" class="form-control" id="">
                            </fieldset>
						<fieldset class="form-group">
							<label for="formGroupExampleInput">Ảnh </label>
							<input   required type="file" name="img" class="form-control" id="">
						</fieldset>
						

						<fieldset class="form-group">
							<label for="formGroupExampleInput">Nội dung tin </label>
							<textarea  required  name="content" id="content"  ></textarea>
							<script>
								CKEDITOR.replace( 'content', {
								filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
								filebrowserImageBrowseUrl:  'ckfinder/ckfinder.html?Type=Images',

								filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
								filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
								filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
								filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								});
							</script>
							
						
							
							
						</fieldset>

						<fieldset class="form-group text-xs-center">
							
							<input class="form-control btn btn-primary" type="submit" value="Thêm"> 
						</fieldset>

						{{ csrf_field()}}
					</form>
				</div>


			</div>
			
        </div>
        <hr>
        <hr>
        <div class="col-sm-12 danhsach">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">DANH SÁCH BÀi VIẾT</h1>
				</div>
			</div><!--/.row-->
			<hr>
			<hr>
				<!-- khoi danh sach tin -->
				<div class="row">
						<style>
								table {
									font-family: arial, sans-serif;
									border-collapse: collapse;
									width: 100%;
								}
								
								td  {
									border: 4px solid #dddddd;
									text-align: left;
									padding: 8px;
								}
								th{
									border: 4px solid #dddddd;
									text-align: center;
									padding: 8px;
									font-size: 20px;
								}
								
								tr:nth-child(even) {
									background-color: #dddddd;
								}
						</style>
								
								
								

								<table>
								  <tr >
									<th>ID</th>
									<th>Tiêu Đề</th>
									<th>Trích Dẫn</th>
									<th>Ảnh</th>
									<th>Tùy Chọn</th>
								  </tr>
								  @if($postlist->count()>0)
									@foreach ($postlist as $post)
										<tr>
											<td>{{ $post->id }}</td>
											<td>{{ $post->title }}</td>
											<td>{{ $post->quote }}</td>
											<td><img class="card-img-top img-fluid" src="{{ asset('storage/app/img/'.$post->img) }}" alt="Card image cap" style="width:100px;height:100px"></td>
											<td><a href=" {{ asset('admin/post/edit/'.$post->id) }} " class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
												<a href=" {{ asset('admin/post/delete/'.$post->id) }} " onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a></td>
										</tr>
									@endforeach
								@else
								<tr>
                                    <th colspan="5" class="text-center" style="font-size:18px;">No published posts</th>
                                </tr>
								@endif
								  
								</table>
								<hr><hr>
	
				</div>
		</div>
		
					
	
	

@stop