@extends('backend.master')
@section('main')

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh mục </h1>
            </div>
        </div><!--/.row-->
        
        
        <div class="row">
            <div class="col-xs-12 col-md-5 col-lg-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Thêm danh mục
                        </div>
                        @include('error.note')
                        <div class="panel-body">
                            <form method="POST" action="{{ asset('admin/cate/add') }}">
                                <div class="form-group">
                                    <label>Tên danh mục:</label>
                                    <input required type="text" name="name" class="form-control" placeholder="Tên danh mục...">
                                </div>
                                <div class="form-group">
                                    <input class="form-control btn btn-primary" type="submit" value="Thêm"> 
                                </div>
                                {{ csrf_field()}}
                            </form>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-md-7 col-lg-7">
                <div class="panel panel-primary">
                    <div class="panel-heading">Danh sách danh mục</div>
                    <div class="col-lg-12">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                            
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-table">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="bg-primary">
                                    <th>Tên danh mục</th>
                                    <th style="width:30%">Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($catelist->count()>0)
                                    @foreach($catelist as $cate)
                                        <tr>
                                            <td>{{ $cate->cate_name }}</td>
                                            <td>
                                                <a href="{{ asset('admin/cate/edit/'.$cate->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
                                                <a href="{{ route('delete',$cate['id']) }}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <th colspan="5" class="text-center">No published cate</th>
                                </tr>

                                @endif


                                
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
@stop