<?php

namespace App\Jobs;

use App\Image as ImageModel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use Image;

class ProcessImageThumbnails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $path;
    protected $fileName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $path,$fileName)
    {
        $this->path = $path;
        
        $this->fileName = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->path;

       // lấy file từ storage/app
        $full_image_path = Image::make(Storage::disk('local')->get('anhgoc/'.$this->fileName));//local = storage/app/ 
        $resized_image_path = public_path($path); //trả về đường dẫn gốc vào public
        $img = \Image::make($full_image_path)->resize(370,250); //tiến hành resize
        $img->save($resized_image_path);//lưu
    }
}
