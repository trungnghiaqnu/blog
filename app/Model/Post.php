<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey ='id';
    protected $fillable = ['id','title','slug','quote','img','content','post_cate'];
}

