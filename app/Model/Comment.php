<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table='comments';
    protected $primarykey='id';
    protected $fillable=['email','name','content','com_post'];
}
