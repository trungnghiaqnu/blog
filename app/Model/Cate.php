<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cate extends Model
{
    protected $table = 'cates';
    protected $primarykey = 'id';
    protected $fillable =['cate_name','cate_slug' ];
}
