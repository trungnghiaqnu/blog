<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cate;
use App\Model\Post;
use App\Model\Comment;
use App\Like;
use Auth;

class FrontendController extends Controller
{
     public function getHome()
    {
        $data['cate'] = Cate::all();
        $data['post'] = Post::orderby('id','DESC')->paginate(10);
        
        return view('frontend/index',$data);
        
        

    
    }
    // public function getDetail($id){
        
    //     $data['catename'] = Cate::where('cate_slug', $id)->first();
    //    
    //     if($data['catename']){
    //         $data['post'] = Post::where('post_cate',$data['catename']->id)->get();
    //         return view('frontend/detail_cate',$data);
    //     }else{
    //         $data['post'] = Post::where('slug', $id)->firstOrFail();
    //         // $data['lienquan'] =baiviet::where('baiviet_cate', $data['baiviet']->baiviet_cate)
    //         // ->where('id','<>',$id)->get();

    //         return view('frontend.detail_post',$data);  
    //    }
      
    // }

    public function getDetail($id ){
        $data['post'] = Post::find($id);

        $likePost = $data['post'];
        $like = Like::where('post_id',$id)->count();
        
        $data['comment'] = Comment::where('com_post',$id)->orderBy('id','desc')->get();
        return view('frontend/detail_post',$data,['like' => $like]);
    } 
    public function getCate($id)
    {
        $data['catename'] = Cate::find($id);
        $data['post_cate'] = Post::where('post_cate',$id)->get();
        return view('frontend/detail_cate',$data);

    }
    public function postComment(Request $request, $id){
        $comment = new Comment;
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->content = $request->content;
        $comment->com_post=$id;
        $comment->save();
        return back();
        
    }
    public function like($id){ 
        $login_user = Auth::user()->id;
        $like_user = Like::where(['user_id' => $login_user, 'post_id' => $id])->first();
    
        if(empty($like_user->user_id)){
            $user_id = Auth::user()->id;
            $email = Auth::user()->email;
            $post_id = $id;

            $like = new Like;
            $like->user_id = $user_id;
            $like->post_id = $post_id;
            $like->email = $email;
            $like->save();
            return redirect()->back();

        }
        else{
            return redirect()->back();
        }
    
    }

}
