<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\addCateRequest;
use App\Http\Requests\editCateRequest;
use App\Model\Cate;

class ApiCateController extends Controller
{
    public function index()
    {
        $data['catelist'] = Cate::all();
        return $data;
    }
 
    public function show($id)
    {
        $data['cate'] = Cate::find($id);

        return $data;
    }

    public function store(Request $request)
    {
        
         return Cate::create($request->all());

    }

    public function update(editCateRequest $request, $id)
    {
        $cate = Cate::findOrFail($id);
        $cate->update($request->all());

        return $cate;
    }

    public function delete(Request $request, $id)
    {
        $cate = Cate::findOrFail($id);
        $cate->delete();

        return 204;
    }
}
