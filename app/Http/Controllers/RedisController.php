<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;
use App\Events\RedisEvent;



class RedisController extends Controller
{
    public function index(){
        $data['message'] = Messages::all();
        
        return view('chat/messages',$data);
    }


    public function postSend(Request $request){
        $message = Messages::create($request->all());

        //tạo event lắng nghe sự kiện
        event(
            $e = new RedisEvent($message)//trả về cho socketio
        );
        return redirect()->back();


    }
}
