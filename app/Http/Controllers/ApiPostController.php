<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\addPostRequest;
use App\Model\Cate;
use App\Model\Post;
use DB;

class ApiPostController extends Controller
{
    public function index()
    {
        $data['postlist'] = DB::table('cates')->join('posts','posts.post_cate','=','cates.id')->get();  
       // $data['catelist'] = Cate::all();
        return $data;
    }
 
    public function show($id)
    {
        $data['post'] = Post::find($id);

        return $data;
    }

    public function store(addPostRequest $request)
    {
        $filename = $request->img->getClientOriginalName();
        $posts = new Post;
        $posts->title= $request->title;
        $posts->slug= str_slug($request->title);
        $posts->quote= $request->quote;
        $posts->content= $request->content;
        $post->img= $filename;
        $posts->post_cate= $request->cate;
       
        $posts->save();
        $request->img->storeAs('img',$filename);
        return $posts;

        // return Post::create($request->all());

    }

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post = new Post;
        
        $arr['title'] = $request->title;
        $arr['slug'] = str_slug($request->title);
        $arr['quote'] = $request->quote;
        $arr['content'] = $request->content;
        $arr['post_cate'] = $request->cate;
        if($request->hasFile('img')){
            $img = $request->img->getclientOriginalName();
            $arr['img'] = $img;
            $request->img->storeAs('img',$img); 
        }
        $post::where('id',$id)->update($arr);
     

        return $post;
    }

    public function delete(Request $request, $id)
    {
        $Post = Post::findOrFail($id);
        $Post->delete();

        return 204;
    }
}

