<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('backend/login');
    }

    public function login(Request $req)
    {
        $this->validate($req,[
            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);
        if (Auth::attempt(['email'=>$req->email, 'password'=>$req->password], $req->remember)) {
            $lv = Auth::user()->level;
            if ($lv==1) {
                return redirect('admin/home');
            }else{
                return redirect('/');
            }
            // 
        }
        return redirect()->back()->withInput($req->only('email', 'remember'));
    }
}
