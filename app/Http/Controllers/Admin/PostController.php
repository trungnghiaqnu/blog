<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Post;
use App\Model\Cate;

use App\Http\Requests\addPostRequest;
use App\Jobs\ProcessImageThumbnails;
use Auth;

use DB,Image;


class PostController extends Controller
{
    public function getPost(){ 
        $data['postlist'] = DB::table('cates')->join('posts','posts.post_cate','=','cates.id')->get();  
           
        $data['catelist'] = Cate::all();
        return view('backend/post',$data);
    }
    public function postAdd(addPostRequest $request){ 
        $posts = new Post;
        $posts->title= $request->title;
        $posts->slug= str_slug($request->title);
        $posts->quote= $request->quote;
        $posts->content= $request->content;
            if($request->hasFile('img')){

                $image = $request->file('img');
                $fileName = $image->getClientOriginalName('img');
                $path = 'storage/app/img/';
                $path2 = 'storage/app/anhgoc/';
                Image::make($image)->resize( 370,250)->save($path.$fileName); 
                
                $image->move($path2,$fileName); 
                $posts->img= $fileName;
                //
                $image->org_path = 'images' . DIRECTORY_SEPARATOR . $fileName; 
                ProcessImageThumbnails::dispatch( $image->org_path, $fileName);
               
            }
        $posts->post_cate= $request->cate;
        $posts->save();
        
        //return redirect('admin/post')->with('flash_message','Bạn Thêm thành công  !!');  
        
        return $posts;
        
    }

    public function getEdit($id){ 
        $data['post'] = Post::find($id);
        $data['catelist'] = Cate::all();
        return view('backend/editpost',$data);
    }

    public function postEdit(Request $request, $id){ 
        $posts = new Post;
        
        $arr['title'] = $request->title;
        $arr['slug'] = str_slug($request->title);
        $arr['quote'] = $request->quote;
        $arr['content'] = $request->content;
        $arr['post_cate'] = $request->cate;
        if($request->hasFile('img')){
            $img = $request->img->getclientOriginalName();
            $arr['img'] = $img;
            $request->img->storeAs('img',$img); 
        }
       
        $posts::where('id',$id)->update($arr);
        return redirect('admin/post')->with('flash_message','Bạn sửa thành công !!'); 
    }

    public function getDelete($id){ 
        Post::destroy($id);
        return back()->with('flash_message','Bạn xóa thành công !!');
        
    }
}
