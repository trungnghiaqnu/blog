<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cate;
use App\Http\Requests\addCateRequest;
use App\Http\Requests\editCateRequest;

class CateController extends Controller
{
    public function getCate(){
       $data['catelist'] = Cate::all();
        return view('backend/cate',$data);
     }
     public function postAddCate(addCateRequest $request){
        $cate = new Cate;
        $cate->cate_name = $request->name;
        $cate->cate_slug = str_slug($request->name);
        $cate-> save();
        return redirect('admin/cate')->with(['flash_message'=>'Bạn thêm thành công']);

    }
    public function getEditCate($id){

        $data['cate'] = Cate::find($id);
        return view('backend/editcate',$data);
    }
    public function postEditCate(editCateRequest $request, $id){

        $cate = Cate::find($id);
        $cate->cate_name = $request->name;
        $cate->cate_slug = str_slug($request->name);
        $cate-> save();
        return redirect('admin/cate')->with(['flash_message'=>'Bạn sửa thành công !!']);
    }
    public function getDeleteCate($id){
        cate::destroy($id);
        return back()->with(['flash_message'=>'Bạn xóa thành công !!']);
    }
}
