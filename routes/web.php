<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontendController@getHome')->name('home');
//Route::get('/{slug?}.html','FrontendController@getDetail','FrontendController@postComment')->where('slug','[ \/\w\.-]*')->name('postDetail');

Route::get('cate/{id}/{slug}.html','FrontendController@getCate');
Route::get('post_detail/{id}/{slug}.html','FrontendController@getDetail' )->name('postdetail');
Route::post('post/{id}/{slug}.html','FrontendController@postComment' );
Route::get('like/{id}/','FrontendController@like' );



Route::group(['namespace'=>'Admin'],function(){ 
    Route::group(['prefix'=>'login','middleware'=>'Checklogin'],function(){
        Route::get('/','LoginController@showLoginForm');
        Route::post('/','LoginController@login');
    });
        Route::get('logout','HomeController@getLogout');

        Route::group(['prefix'=>'admin','middleware'=>'Checklogout'],function(){
                Route::get('home','HomeController@getHome');

                // cate
                Route::group(['prefix'=>'cate'],function(){
                    Route::get('/','CateController@getCate');
                    Route::post('add','CateController@postAddCate');
                    Route::get('edit/{id}','CateController@getEditCate');
                    Route::post('edit/{id}','CateController@postEditCate');
                    Route::get('delete/{id}','CateController@getDeleteCate')->name('delete');
                });

                
                //post
                Route::group(['prefix'=>'post'],function(){
                    Route::get('/','PostController@getPost');

                    Route::post('add','PostController@postAdd');

                    Route::get('edit/{id}','PostController@getEdit');
                    Route::post('edit/{id}','PostController@postEdit');

                    Route::get('delete/{id}','PostController@getDelete');
                
                });
            });

        });





Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');



Route::get('send-message', 'RedisController@index');
Route::post('send-message', 'RedisController@postSend');

