<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('post', 'ApiPostController@index');
Route::get('post/{id}', 'ApiPostController@show');
Route::post('post', 'ApiPostController@store');
Route::put('post/{id}', 'ApiPostController@update');
Route::delete('post/{id}', 'ApiPostController@delete');


Route::get('cate', 'ApiCateController@index');
Route::get('cate/{id}', 'ApiCateController@show');
Route::post('cate', 'ApiCateController@store');
Route::put('cate/{id}', 'ApiCateController@update');
Route::delete('cate/{id}', 'ApiCateController@delete');
